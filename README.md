# Gatsby Oracle #

Stores code for the Gatsby Oracle

### Description ###

* Repo for all the iterations of the Gatsby Oracle. 
* The Oracle purpose is to give Gatsby smart contracts the power to interact outside the smart contract

### How do I get set up? ###

* Make sure you download nodejs. Have npm. 
* Check with “npm --version”
* Have Python 2.7. Get this by using this command : npm install --global --production windows-build-tools
* This gives you all necessary tools to install web3 and other nodejs applications
* Download truffle
* “npm truffle -g install”
* Download ethereum local test chain
* “npm install -g ethereumjs-testrpc”
* Copy Code
* Type Testrpc on terminal or CP (Command prompt) to start test environment
* Open another CP or terminal. Run node oracle.js (the oracle running)
* Open another CP or terminal. Run node client.js (mirrors a call from a client) 


### Sources ###

* https://kndrck.co/posts/ethereum_oracles_a_simple_guide/ 