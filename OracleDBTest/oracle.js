var fetch = require('fetch')
var OracleContract = require('./build/contracts/CMCOracle.json')
var contract = require('truffle-contract')

// Need to create the var for the database:
var AWS = require('aws-sdk');
let awsConfig = {
  "region": "us-east-1",
  "endpoint": "http://dynamodb.us-east-1.amazonaws.com",
  "accessKeyId": "AKIAIRBD2GI5JHG3JT3Q", "secretAccessKey": "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"
};
AWS.config.update(awsConfig);

var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));



// Truffle abstraction to interact with our
// deployed contract
var oracleContract = contract(OracleContract)
oracleContract.setProvider(web3.currentProvider)

// Dirty hack for web3@1.0.0 support for localhost testrpc
// see https://github.com/trufflesuite/truffle-contract/issues/56#issuecomment-331084530
if (typeof oracleContract.currentProvider.sendAsync !== "function") {
  oracleContract.currentProvider.sendAsync = function() {
    return oracleContract.currentProvider.send.apply(
      oracleContract.currentProvider, arguments
    );
  };
}

// Get accounts from web3
web3.eth.getAccounts((err, accounts) => {
  oracleContract.deployed()
  .then((oracleInstance) => { /*
    // Watch event and respond to event
    // With a callback function
    oracleInstance.CallbackGetBTCInfo()
    .watch((err, event) => {
      // Fetch data
      // and update it into the contract
      fetch.fetchUrl('https://api.coinmarketcap.com/v1/global/', (err, m, b) => {
        const cmcJson = JSON.parse(b.toString())
        const btcMarketCap = parseInt(cmcJson.total_market_cap_usd)
        const volumeUSD = parseInt(cmcJson.total_24h_volume_usd)
        const actCur = parseInt(cmcJson.active_currencies)

        // Send data back contract on-chain
        oracleInstance.setBTCCap(btcMarketCap, {from: accounts[0]})
        oracleInstance.setVolume(volumeUSD, {from: accounts[0]})
        oracleInstance.setActCurr(actCur, {from: accounts[0]})
      })
    })*/
    // Potential code to test to send the other way
    // (create a deposit event)
    oracleInstance.CallbackDeposit()
    .watch((err, event) => {
          /* Creating a function to write stuff to x.
          - To send it I need create a new var on top that leads to a different
          - provider and give it to that provider. I'm going to need to deploy it too?
          -  doesn't need to be web3. Need to learn how to send data.
          */
        let docClient = new AWS.DynamoDB.DocumentClient();
        var status = oracleInstance.getStatus({from: accounts[0]});
        console.log(status);
        var eth_addy = oracleInstance.getSender({from: accounts[0]});
        console.log(status);
        let updateDB = function () {
          var input = {
            "IP address": "192.167.7.1", "eth_address": eth_addy, "status": status,
            "deposit_on": new Date().toString()
          };
          var params = {
            TableName: "OracleDBTest",
            Item: input
          };
          docClient.put(params, function (err, data) {
            if (err) {
              console.log("users::updateDB::error - " + JSON.stringify(err, null, 2));
            } else {
              console.log("user::updateDB::success");
            }
          });
        }
        updateDB();
    })
  })
  .catch((err) => {
    console.log(err)
  })
})
