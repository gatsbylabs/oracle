pragma solidity ^0.4.17;

contract CMCOracle {
  // Contract owner
  address public owner;

  // BTC info Storage
  uint public btcMarketCap;
  uint public totVolumeUSD;
  uint public actCurr;

  mapping(address => bool) public _status;

  // Callback function
  event CallbackGetBTCInfo();
  event CallbackDeposit();

  function CMCOracle() public {
    owner = msg.sender;
  }

  function updateBTCInfo() public {
    // Calls the callback function
    CallbackGetBTCInfo();
  }

  function deposit() public {
    CallbackDeposit();
  }

  function setStatusTrue() public {
    _status[msg.sender] == true;
  }

  function getStatus() public returns(bool) {
    return _status[msg.sender];
  }

  function getSender() public returns(address) {
    return msg.sender;
  }


  function setBTCCap(uint cap) public {
    // If it isn't sent by a trusted oracle
    // a.k.a ourselves, ignore it
    require(msg.sender == owner);
    btcMarketCap = cap;
  }

  function setVolume(uint volume) public {
    require(msg.sender == owner);
    totVolumeUSD = volume;
  }

  function setActCurr(uint activeCurr) public {
    require(msg.sender == owner);
    actCurr = activeCurr;
  }

  function getBTCCap() constant public returns (uint) {
    return btcMarketCap;
  }

  function getVolume() constant public returns (uint) {
    return totVolumeUSD;
  }

  function getActCurr() constant public returns (uint) {
    return actCurr;
  }

}
