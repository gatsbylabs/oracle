var OracleContract = require('./build/contracts/CMCOracle.json')
var contract = require('truffle-contract')

var AWS = require('aws-sdk');
let awsConfig = {
  "region": "us-east-1",
  "endpoint": "http://dynamodb.us-east-1.amazonaws.com",
  "accessKeyId": "AKIAIRBD2GI5JHG3JT3Q", "secretAccessKey": "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"
};
AWS.config.update(awsConfig);

var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));

// Truffle abstraction to interact with our
// deployed contract
var oracleContract = contract(OracleContract)
oracleContract.setProvider(web3.currentProvider)

// Dirty hack for web3@1.0.0 support for localhost testrpc
// see https://github.com/trufflesuite/truffle-contract/issues/56#issuecomment-331084530
if (typeof oracleContract.currentProvider.sendAsync !== "function") {
  oracleContract.currentProvider.sendAsync = function() {
    return oracleContract.currentProvider.send.apply(
      oracleContract.currentProvider, arguments
    );
  };
}

web3.eth.getAccounts((err, accounts) => {
  oracleContract.deployed()
  .then((oracleInstance) => {
    // Our promises
    const oraclePromises = [
      //oracleInstance.getBTCCap(),  // Get currently stored BTC Cap
      //oracleInstance.getVolume(),
      //oracleInstance.getActCurr(),
      //oracleInstance.updateBTCInfo({from: accounts[0]}),  // Request oracle to update the information
      oracleInstance.setStatusTrue({from: accounts[0]}),
      oracleInstance.deposit({from: accounts[0]}),
      oracleInstance.getStatus({from: accounts[0]})
    ]

    // Map over all promises
    Promise.all(oraclePromises)
    .then((result) => {
      //console.log('BTC Market Cap: ' + result[0])
      //console.log('BTC Total 24h Volume USD: ' + result[1])
      //console.log('Active Currency: ' + result[2])
      console.log('Deposit Status: ' + result[0])
      console.log('Requesting Oracle to update CMC Information...')
    })
    .catch((err) => {
      console.log(err)
    })
  })
  .catch((err) => {
    console.log(err)
  })
})
