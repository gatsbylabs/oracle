pragma solidity ^0.4.17;

contract CMCOracle {
  // Contract owner
  address public owner;

  mapping(address => bool) public _status;

  // Callback function for deposit that address and bool to js
  // event CallbackGetBTCInfo();
  event CallbackDeposit(address _from, bool _status);

  function CMCOracle() public {
    owner = msg.sender;
  }

  function deposit() public {
    emit CallbackDeposit(msg.sender, true);
  }

  function setStatusTrue() public {
    _status[msg.sender] == true;
  }

  function getStatus() public returns(bool) {
    return _status[msg.sender];
  }
}
