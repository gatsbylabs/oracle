var fetch = require('fetch')
var OracleContract = require('./build/contracts/CMCOracle.json')
var contract = require('truffle-contract')

// Need to create the var for the database:
var AWS = require('aws-sdk');
let awsConfig = {
  "region": "us-east-1",
  "endpoint": "http://dynamodb.us-east-1.amazonaws.com",
  "accessKeyId": "AKIAIRBD2GI5JHG3JT3Q", "secretAccessKey": "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"
};
AWS.config.update(awsConfig);

var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));



// Truffle abstraction to interact with our
// deployed contract
var oracleContract = contract(OracleContract)
oracleContract.setProvider(web3.currentProvider)

// Dirty hack for web3@1.0.0 support for localhost testrpc
// see https://github.com/trufflesuite/truffle-contract/issues/56#issuecomment-331084530
if (typeof oracleContract.currentProvider.sendAsync !== "function") {
  oracleContract.currentProvider.sendAsync = function() {
    return oracleContract.currentProvider.send.apply(
      oracleContract.currentProvider, arguments
    );
  };
}

// Get accounts from web3
web3.eth.getAccounts((err, accounts) => {
  // Deploying contract
  oracleContract.deployed()
  .then((oracleInstance) => {
    // Watching deposit event
    oracleInstance.CallbackDeposit()
    .watch((err, event) => {

        let docClient = new AWS.DynamoDB.DocumentClient();
        // Grabbing status and address from smart contract
        var eth_addy = event.args._from;
        var status = event.args._status;
        console.log(status);
        console.log(eth_addy);
        // function to write information to dynamo database
        let writeDB = function () {
          var input = {
            "IP address": "192.167.14.1", "status": status, "eth_address": eth_addy,
            "deposit_on": new Date().toString()
          };
          var params = {
            TableName: "OracleDBTest",
            Item: input
          };
          docClient.put(params, function (err, data) {
            if (err) {
              console.log("users::updateDB::error - " + JSON.stringify(err, null, 2));
            } else {
              console.log("user::updateDB::success");
            }
          });
        }
        writeDB();
    })
  })
  .catch((err) => {
    console.log(err)
  })
})
